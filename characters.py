#Character classes in the game
class Character:
    def __init__(self, name):
        self.name = name
        self.sample = []

    def __str__(self):
        return f"I' am  the capitan. My name is {self.name} and thi is my expedition."

    def get_sample(self, item):
        self.sample.append(item)

    def show_items(self):
        return self.sample

class Alien(Character):
    def __str__(self):
        return f" I' am alien. My name is {self.name} \
and I'll kill you and take my treasures away"
        
# Class instances
capitan = Character("Molossus")
ostrodus = Alien("Ostrodus")
