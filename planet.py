
# In this section you will find descriptions of the planet
def planet():
    print("\t You are an astronaut Captain Molossus who has flown\
with his crew to the planet Merkus. The goal is to explore the planet,\
collect samples, and return to Earth.\n \t There is a crater on the edge \
of which are piled up with unusual stones. There is liquid inside the crater.\
The entire crater is overgrown with unusual firefly flowers.")

second_part = "\n Congratulations, you have won all the samples. \
You return to the spaceship. The moment you stand on the entrance ramp, \
you notice something strange. In the moonlight, the figure of an alien \
can be seen. His skin is blue and his eyes shine like diamonds. \
In his hand, he holds what appears to be a weapon and he's talking to you. \n"

choice = "Make a quick decision. You have two Options: \n \
1. Pull out your own weapons and fight back \n \
2. Drop the samples and escape to the ship "

# classes describing the elements of a planet
class World:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return f"This is the planet name: {self.name}"

class Crater(World):
    species = "crater"

    def __str__(self):
        return f"The crater is a place with an extraordinary history \
and mysterious origins. As you stand on its edge, you feel  \
strange vibrations and hear the whisper of nature. \
At the bottom of the crater is an unusual liquid. \
The crater has a diameter of about 2 kilometers and is more than 500 meters deep. \
You need a cylinder to get the samples"
    

class Stones(World):
    species = "stones"

    def __str__(self):
        return f"Diamond stones are located on the outskirts of the crater. \
They are about 20 cm in size and you need a sack to take them."
    

class Flowers(World):
    species = "flowers"

    def __str__(self):
        return f"Firefly flowers bloom on the crater rims. \
Their petals glow at night, creating a magical landscape. \
If you want to take one with you, you need a pair of scissors. "

#Class Instances
merkus = World("Merkus")
crater = Crater("Crater")
stones = Stones("Stones")
flowers = Flowers("Firefly flowers")

