import planet
import tools
import characters


# The game function compares the selection of the decision with tool
def game():
       
    while len(characters.capitan.sample) < 3:
        decision = input("Please Write what you want to explore: crater, stones or flowers: ")
        tools.desc()
        tools.desc_tool()
        tool = input("Choose the tool you need: cylinder, sack, shears : ")  
    
        if tool.lower() == "cylinder" and decision.lower() == "crater":
            print("Great, good job, you have a sample of the liquid")
            characters.capitan.get_sample("liquid")
            print("Bravo added item to the list", characters.capitan.show_items())
        elif tool.lower() == "sack" and decision.lower() == "stones":
            print("Great, good job, you have a stone")
            characters.capitan.get_sample("stone")
            print("Bravo added item to the list", characters.capitan.show_items())
        elif tool.lower() == "shears" and decision.lower() == "flowers":
            print("Great, good job, you have a flower")
            characters.capitan.get_sample("flower")
            print("Bravo added item to the list", characters.capitan.show_items())
        else:
            print("try again")


planet.planet()


# Program control loop
while True:

    game()
         
    print(planet.second_part)
    print(characters.ostrodus)
    print(planet.choice)
    
    decide = input("Press 1 or 2: ")
    if int(decide) == 1:
        print("Congratulations you have collected all the samples, \
    You've defeated the alien, you can go home")
        break
    elif int(decide) == 2:
        print("GAME OVER.")
        print("You survived, but you lost all your samples. You have to start over")
        break
    else:
        print("Press 1 or 2")
            
        
