import planet

# Classes describing the tools
class Tools:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return f"This is the {self.name}"

class Shears(Tools):
    species = "grass shears"

    def __str__(self):
        return f"These are scissors with which you can cut down vegetation, \
e.g. a flower, to take it to the ground"

class Sack(Tools):
    species = "sacks"

    def __str__(self):
        return f"The sack can be used to hide stones"

class Cylinder(Tools):
    species = "cylinder"

    def __str__(self):
        return f"You can use the cylinder tube to secure the liquid taken \
from the bottom of the crater"


# Class instances
sack = Sack("sack")
cylinder = Cylinder("cylinder")
shears = Shears("shears")

# Fuction describing the tools
def desc_tool():
    desc_tool = input("If you need a description of tools, type tool: ")
    if desc_tool.lower() == "tool":
        print(cylinder, '\n' , 50 * "*")
        print(sack,  '\n' , 50 * "*")
        print(shears,  '\n' , 50 * "*")

# Function describing the samples
def desc():
    desc = input("If you need a description of samples, type desc: ")
    if desc.lower() == "desc":
        print(planet.merkus, '\n' , 50 * "*")
        print(planet.crater,  '\n' , 50 * "*")
        print(planet.stones,  '\n' , 50 * "*")
        print(planet.flowers,  '\n' , 50 * "*")
